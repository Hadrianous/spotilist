import spotipy
from lists.services.spotipy_proxy import SpotipyProxy
from django.shortcuts import render, redirect
from django.contrib import messages
from lists.services.redis import RedisClient

def playlist_unduplicate(request):
    if "user_id" not in request.session:
        return redirect('/login')

    redis_client = RedisClient()
    token = redis_client.getRedis().get(request.session['user_id'])

    if not token:
        return redirect('/login')
    else:
        token = token.decode('utf-8')

    spotipy_proxy = SpotipyProxy(token)

    if token:
        try:
            playlist_id = request.POST.get('playlist_id')
            if playlist_id is not None:
                num_duplicated = spotipy_proxy.remove_duplicated_tracks(request.session['user_id'], playlist_id)
                messages.info(request, 'Nombre de morceaux dédoublonnées : ' + str(num_duplicated))
        except spotipy.SpotifyException as ex:
            print(ex)
            messages.warning(request, ex)
            return redirect('/playlists_unduplicate')
        except Exception as ex:
            print(ex)
            messages.warning(request, ex)
            return redirect('/playlists_unduplicate')

    response = spotipy_proxy.get_spotify().user_playlists(request.session['user_id'])

    return render(
        request,
        "lists/duplicate.html",
        {
            "playlists": response['items']
        }
    )