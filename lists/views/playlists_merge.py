import spotipy
from lists.services.spotipy_proxy import SpotipyProxy
from django.shortcuts import render, redirect
from django.contrib import messages
from lists.services.redis import RedisClient


def playlists_merge(request):
    if "user_id" not in request.session:
        return redirect('/login')

    redis_client = RedisClient()
    token = redis_client.getRedis().get(request.session['user_id'])

    if not token:
        return redirect('/login')
    else:
        token = token.decode('utf-8')

    spotipy_proxy = SpotipyProxy(token)
    nb_tracks_merged = None
    if token:
        try:
            playlist_from_id = request.POST.get('from')
            playlist_to_id = request.POST.get('to')
            
            if playlist_to_id is not None and playlist_from_id is not None:
                from_tracks = spotipy_proxy.get_all_playlists_tracks(request.session['user_id'], playlist_from_id, 100)
                if from_tracks:
                    to_tracks = spotipy_proxy.get_all_playlists_tracks(request.session['user_id'], playlist_to_id, 100)
                    from_tracks_ids = []
                    to_tracks_ids = []
                    final_tracks = []

                    [from_tracks_ids.append("spotify:track:" + track_data_from['track']['id']) for track_data_from in from_tracks]
                    [to_tracks_ids.append("spotify:track:" + track_data_to['track']['id']) for track_data_to in to_tracks]

                    # remove duplicated tracks that are already in destination playlist
                    [final_tracks.append(item) for item in from_tracks_ids if item not in to_tracks_ids]
                    nb_tracks_merged = len(final_tracks)

                    if final_tracks:
                        tracks_arrays = _split(final_tracks, 100) # divide tracks in smaller chunks to send to spotify
                        for sub_tracks in tracks_arrays:
                            spotipy_proxy.get_spotify().user_playlist_add_tracks(request.session['user_id'], playlist_to_id, sub_tracks)

                messages.info(request, 'Nombre de pistes fusionnées : ' + str(nb_tracks_merged))

            response = spotipy_proxy.get_spotify().user_playlists(request.session['user_id'])
            return render(
                request,
                "lists/playlists_merge.html",
                {
                    "playlists": response['items']
                }
            )
        except spotipy.SpotifyException:
            return redirect('/login')
        except Exception as ex:
            print(ex)
            messages.warning(request, ex)
            return redirect('/playlists')


def _split(arr, size):
    new_array = []
    while len(arr) > size:
        piece = arr[:size]
        new_array.append(piece)
        arr = arr[size:]
    new_array.append(arr)
    return new_array
