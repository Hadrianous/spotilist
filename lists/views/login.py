import spotipy
import spotipy.oauth2 as oauth2
import urllib.parse as urlparse
from urllib.parse import parse_qs
from django.shortcuts import redirect
from lists.services.redis import RedisClient
import configparser


def login(request):
    access_token = ""
    scope = 'playlist-modify-public playlist-modify-private'

    config = configparser.ConfigParser()
    config.read('./List/config/config.ini')

    sp_oauth = oauth2.SpotifyOAuth(
        config['spotify']['client_id'],
        config['spotify']['client_secret'],
        'http://127.0.0.1:8000/login/',
        scope=scope
    )

    redis_client = RedisClient()

    if 'user_id' in request.session:
        access_token = redis_client.getRedis().get(request.session['user_id'])

    if not access_token:
        print('not token in cache, get from url')
        url = request.build_absolute_uri()
        parsed = urlparse.urlparse(url)
        qs = parse_qs(parsed.query)
        if "code" in qs:
            token_info = sp_oauth.get_access_token(qs['code'][0])
            if token_info:
                access_token = token_info['access_token']

    if access_token:
        print('token found')
        try:
            sp = spotipy.Spotify(access_token)
            user_data = sp.current_user()
        except spotipy.SpotifyException as ex:
            print(ex)
            if ex.http_status == 401:
                redis_client.getRedis().delete(request.session['user_id'])
            return redirect('/login')

        request.session['user_id'] = user_data['id']
        redis_client = RedisClient()
        redis_client.getRedis().set(user_data['id'], access_token)
        return redirect('/')
    else:
        print('not token in cache, get spotify')
        auth_url = sp_oauth.get_authorize_url()
        return redirect(auth_url)
