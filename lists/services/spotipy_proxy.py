import spotipy


class SpotipyProxy:

    def __init__(self, token):
        self.__token = token
        self.__spotify = None

    def get_spotify(self):
        if self.__spotify is None:
            self.__spotify = spotipy.Spotify(self.__token)

        return self.__spotify

    def get_all_playlists_tracks(self, user_id: str, playlist_id: str, limit: int = 100) -> list:
        response = self.get_spotify().user_playlist_tracks(user_id, playlist_id, limit=limit)
        tracks = response['items']

        while len(tracks) < response['total']:
            response = self.get_spotify().user_playlist_tracks(user_id, playlist_id, limit=limit, offset=len(tracks))
            tracks.extend(response['items'])

        return tracks

    def remove_duplicated_tracks(self, user_id: str, playlist_id: str) -> int:
        response_tracks = self.get_all_playlists_tracks(user_id, playlist_id)

        tracks = []
        [tracks.append("spotify:track:" + track_data['track']['id']) for track_data in response_tracks]

        deduplicated_tracks = []
        [deduplicated_tracks.append(item) for item in tracks if item not in deduplicated_tracks]

        tracks_array = self._split(tracks, 100)  # divide tracks in smaller chunks to send to spotify
        deduplicated_tracks_array = self._split(deduplicated_tracks, 100)  # divide tracks in smaller chunks to send to spotify

        for sub_tracks in tracks_array:
            self.__spotify.user_playlist_remove_all_occurrences_of_tracks(user_id, playlist_id, sub_tracks)

        for sub_tracks in deduplicated_tracks_array:
            self.__spotify.user_playlist_add_tracks(user_id, playlist_id, sub_tracks)

        return len(tracks) - len(deduplicated_tracks)

    def _split(self, arr, size):
        new_array = []
        while len(arr) > size:
            piece = arr[:size]
            new_array.append(piece)
            arr = arr[size:]
        new_array.append(arr)
        return new_array