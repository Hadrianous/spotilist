import redis


class RedisClient:
    __r = None

    def __init__(self):
        self.__r = redis.Redis('redis')

    def getRedis(self):
        return self.__r
