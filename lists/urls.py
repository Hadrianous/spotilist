from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^login/', views.login),
    url(r'^playlists/$', views.playlists_merge),
    url(r'^playlists_unduplicate/$', views.playlist_unduplicate)
]
